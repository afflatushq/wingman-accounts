(ns wingman-accounts.handler
  (:gen-class)
  (:use org.httpkit.server)
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [wingman-accounts.users :as users]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [ring.util.response :refer [response]]
            [clojure.walk :refer [keywordize-keys]]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]))

(defroutes app-routes
  (GET "/" [] "Hello World")

  (POST "/login" {body :body params :params}
    (response (users/login (keywordize-keys body))))

  (GET "/location/:user-id" [user-id]
    (response (users/get-location user-id)))

  (POST "/signup" {body :body params :params}
    (response (users/sign-up (keywordize-keys body))))

  (POST "/set-pref" {body :body params :params}
    (response (users/update-preference (keywordize-keys body))))

  (route/not-found "Not Found"))

(def app
  (-> app-routes
      wrap-json-body
      wrap-json-response
      (wrap-defaults api-defaults)))

(defn -main []
  (run-server app {:port 4040}))
