(ns wingman-accounts.users
  (:require [rethinkdb.query :as r]))

(defmacro rethink-> [& stmts]
  `(with-open [conn# (r/connect :host "127.0.0.1" :port 28015 :db "test")]
     (-> ~@stmts
         (r/run conn#))))

(defn login [{:keys [name password]}]
  (let [users (rethink-> (r/table "users")
                         (r/get-all [name] {:index "name"}))]
    {:success (= (:password (first users)) password)}))

(defn- fetch-user [{:keys [userId]}]
  (rethink-> (r/table "users")
             (r/get-all [userId] {:index "userId"})))

(defn sign-up [user-data]
  (if (empty? (fetch-user user-data))
    (rethink-> (r/table "users")
               (r/insert [user-data]))))

(defn get-location [id]
  (let [user (first (fetch-user {:userId id}))
        [lat long] (get-in user [:location :coordinates])]
    (prn user)
    {:latitude lat
     :longitude long}))

(defn update-preference [{:keys [user-id preference]}]
  (rethink-> (r/table "users")
             (r/get-all [user-id] {:index "userId"})
             (r/update (r/fn [user]
                         {:preference preference}))))
